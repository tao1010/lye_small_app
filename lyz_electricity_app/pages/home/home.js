// pages/home/home.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    motto: 'Hello World-test',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          console.log(res.userInfo);
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },
  getUserInfo: function (e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    console.log("=============");
    //获取accessToken
    

    // var apiString = "https://api.weixin.qq.com/wxa/getnearbypoilist?page=1&page_rows=20&access_token=ACCESS_TOKEN";
    // wx.request({
    //   url: apiString, 
    //   data: {
    //     access_token: '',
    //     page: '1',
    //     page_rows:'100'
    //   },
    //   header: {
    //     'content-type': 'application/json' // 默认值
    //   },
    //   success(res) {
    //     console.log(res.data)
    //   }
    // })

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    

  },
  //获取token值
  getAccessTokenAction:function(){


  },
  loginAction:function (){
    console.log('============================');
  },
  checkNearSamllAppAction: function () {
    console.log("--------------");
    // wx.navigateTo({
    //   url: '../smallAppList/smallAppList',
    // })

    // wx.showLoading({
    //   title: '加载中...',
    // })
    // wx.hideLoading();
    //获取附近的小程序
    wx.request({
        url: "http://119.23.149.7:9999/app/goods/hot/list",
        data:{
          userId:"851",
          identityType:"0",
          page:"1",
          size:"4"
        },
        method:"POST",
        header:{
          "Authorization": app.globalData.token,
          // "content-type":"application/json"
        },
        success: function (res) {
          console.log('===============' + JSON.stringify(res.data))
        },
        fail: function (res) {
          console.log('``````````````'+JSON.stringify(res))
        }

      })


   
    
    //登录
    // wx.request({
    //   url:api+"User/login",
    //   data:{
    //     username:"18582598001",
    //     password:"111111"

    //   },
    //   method:"POST",
    //   header:{

    //     "content-type":"application/json"
    //   },
    //   success: function (res) {
    //     console.log(res.data)
    //   }
    // })

  },


  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})