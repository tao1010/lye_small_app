// pages/goods/goods.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getGoodsList();
  },
  // 获取商品列表
  getGoodsList:function(){
    var that = this
    var paramsDic = {
      "firstCategoryCode":"oil",
      "categoryId":"",
      "brandId":"",	
      "typeId":"",
      "specification"	:"",
      "userId":"851",
      "identityType":"0",
      "page":	1,
      "size":	8
    };
    wx.request({
      url: app.globalData.serverUrl +'/app/goods/filter',
      data: paramsDic,
      method: "POST",
      header: {
        "Authorization": app.globalData.token,
        "content-type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        if (res.data.code == 0){
          if (res.data.content && res.data.content.list){
            console.log('===============' + JSON.stringify(res.data))
            var goodsList = res.data.content.list;
            that.setData({
              list: goodsList
            })
          }
        }else{
          wx.showToast({
            title: e.data.message,
          })
        }
      },
      fail: function (res) {
        console.log('===============' + JSON.stringify(res))
      },

    })
  },
  //查看商品详情
  checkGoodsDetailsAction: function(e){
    console.log(JSON.stringify(e));
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})