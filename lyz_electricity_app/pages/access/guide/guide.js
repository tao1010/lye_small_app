// pages/guide/guide.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    guideSrc: '../../images/common/lyz_guide.png',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.getStorage({
      key: 'key',
      success(res) {
        console.log(res.data)
      }
    })
  },
  //账号 - 自动登录
  accessLoginAction: function(){
    var paramsDic = {
      "name": this.data.access,
      "password": passwordEncode,
      "deviceId": deviceId,
      "systemType": 'IOS',
      "clientId": cliented
    };
    wx.showLoading({
      title: '登录中...',
    });
    console.log(JSON.stringify(paramsDic));
    wx.request({
      url: app.globalData.serverUrl + "/app/employee/login",
      data: paramsDic,
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      method: "POST",
      success: function (e) {
        wx.hideLoading();
        console.log('data' + JSON.stringify(e));
        if (e.data.code == 0) {
          if (e.header && e.header.token) {
            app.globalData.token = e.header.token;
          }
          if (e.data.content) {
            app.globalData.userInfo = e.data.content;
          }
          // 保存到本地数据 
          wx.setStorage({
            logoInfo: JSON.stringify(paramsDic),
          })
          wx.reLaunch({
            url: "../../home/home",
          })
          wx.showToast({
            title: '登录成功',
            icon: 'success',
          })
        } else {
          wx.showToast({
            title: e.data.message,
          })
        }
      },
      fail: function (e) {
        console.log(JSON.stringify(e));
        wx.hideLoading();
        wx.showToast({
          title: e.data.message,
        })
      }
    })

  }
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  // 自定义点击事件
  accessLoginAction:function(){
    wx.navigateTo({
      url: '../login/login',
    })
  },
  weixinLoginAction:function(){
    console.log('----------');
    wx.reLaunch({
      url: "../../home/home",
    })
  }

})