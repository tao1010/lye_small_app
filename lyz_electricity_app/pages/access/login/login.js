// pages/access/login/login.js
const base64 = require('../../../utils/base64.js')
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    access:null,
    password:null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  accessLoginAction:function(){
    if (!this.data.access){
      wx.showToast({
        title: '请输入登录账号',
        image: '../../../images/common/lyz_error.png',
      })
      return false;
    }
    if (!this.data.password) {
      wx.showToast({
        title: '请输入登录密码',
        image: '../../../images/common/lyz_error.png',
      })
      return false;
    }
      var deviceId = 'F5184225-DEAA-454E-9670-3CB2A12F106E';
      var cliented = '2a6949289914e07285a00b75f0753f59';
      var passwordEncode = base64.encode(this.data.password);
      var paramsDic = {
        "name": this.data.access, 
        "password": passwordEncode, 
        "deviceId": deviceId, 
        "systemType": 'IOS',
        "clientId": cliented};
      wx.showLoading({
        title: '登录中...',
      });
      console.log(JSON.stringify(paramsDic));
      wx.request({
        url: app.globalData.serverUrl+"/app/employee/login",
        data:paramsDic,
        header: {
          "content-type": "application/x-www-form-urlencoded"
        },
        method: "POST",
        success:function(e){
          wx.hideLoading();
          console.log('data'+JSON.stringify(e));
          if(e.data.code == 0){
            if(e.header && e.header.token){
              app.globalData.token = e.header.token;
            }
            if(e.data.content){
              app.globalData.userInfo = e.data.content;
            }
            // 保存到本地数据 
            wx.setStorage({
              key: 'logoInfo',
              data: JSON.stringify(paramsDic),
            })
            wx.reLaunch({
              url: "../../home/home",
            })
            wx.showToast({
              title: '登录成功',
              icon: 'success',
            })
          }else{
            wx.showToast({
              title: e.data.message,
            })
          }
        },
        fail:function(e){
          console.log(JSON.stringify(e));
          wx.hideLoading();
          wx.showToast({
            title: e.data.message,
          })
        }
      })

  },


  // 取值
  getAccess: function (e) {
    var that = this;
    that.setData({
      access: e.detail.value
    });
  },
  getPassword:function(e){
    var that = this;
    that.setData({
      password: e.detail.value
    });
  }
})