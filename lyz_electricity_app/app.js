//app.js
const app = getApp();
const appId = "wx933aa61d2fda8d42";
const appSecret = "bb7d0c89448358bd8f760232f2be82a4"; 

App({
  //serverUrl = 'http://app.leyizhuang.com.cn',
  // oCms = 'http://120.77.14.185:6789',//CMS
  // testUrl = 'http://119.23.149.7:9999',
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        console.log('登录接口返回数据' + JSON.stringify(res));
        if(res.code){
          //app.globalData.code = res.code;
          //通过code获取session_key
          var apiUrl = "https://api.weixin.qq.com/sns/jscode2session?appid=" + appId + "&secret=" + appSecret + "&js_code=" + res.code + "&grant_type=authorization_code;"
          //apiUrl = "https://api.weixin.qq.com/sns/jscode2session";
          var paramDic = { 'appid': appId, 'secret': appSecret, 'js_code': res.code, 'grant_type':'authorization_code'};
          wx.request({
            url: apiUrl,
            data: {},
            method: "POST", 
            header: { 'content-type': 'application/json' }, 
            success: function (res) {
              console.log("获得微信登录SessionKey=",JSON.stringify(res));
              //获取access_token
              // wx.request({
              //   url: '',
              //   data:{},
              //   method:'GET',
              //   header:{ 'content-type': 'application/json' }, 
              //   success:function(res){

              //   },
              // });
            }
          });
        }else{
          wx.showToast({
            title: '登录失败！',
            icon: 'none',
          });
        }
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo
              console.log(res.code);
              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },
  globalData: {
    token: null,//登录信息
    serverUrl: 'http://app.leyizhuang.com.cn',
    userInfo: null,
    code:null,//有效期5分钟
    

  }
})